---
layout: markdown_page
title: "Category Direction - Authorization and Authentication"
---

- TOC
{:toc}

| Category | **Authorization and Authentication** |
| --- | --- |
| Stage | [Manage](https://about.gitlab.com/handbook/product/categories/#manage-stage) |
| Group | [Access](https://about.gitlab.com/handbook/product/categories/#access-group) |
| Maturity | [Complete](/direction/maturity/) |
| Content Last Reviewed | `2020-02-12` |

## Introduction and how you can help

Thanks for visiting the direction page for Authentication and Authorization in GitLab. This page is being actively maintained by the Product Manager for the [Access group](https://about.gitlab.com/handbook/product/categories/#access-group). If you'd like to contribute or provide feedback on our category direction, you can:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/1983).
   * We're especially interested in your experience if you're a large organization that's invested in centralized identity management and have suggestions on how GitLab could better support your needs.
1. Find issues in this category accepting merge requests. [Here's an example query](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=authentication&label_name[]=Accepting%20merge%20requests).

## Overview

Authentication and authorization are critical, foundational elements to keeping resources secure but accessible. This is of particular importance for large enterprises or companies operating in regulated environments; for an authentication strategy to function, it must be secure and scalable. Onboarding/offboarding new employees should be automatable and not allow unauthorized users to access sensitive information.

Furthermore, security strategies have evolved from one of assumed trust within a trusted network. Historically, most organizations assume that authenticated users coming from a trusted source are allowed to access resources - those outside, if not on a VPN, cannot. However, security perimeters have become increasingly porous and an assumed trust model begins to break down with more services shifting to cloud and more employees BYODing or working remotely. 

While GitLab's supported strategies meet most expectations (especially outside of the enterprise), our strategy is to aggressively invest in further improvements to authentication and authorization, with a particular focus on SAML.

### Target audience and experience

The primary audience for future effort are administrators in medium to large enterprises. These are privileged, sophisticated users in companies managing employee identities with a single source of truth; this may be a series of LDAP servers or an IdAAS service like Okta. Automation matters - we should minimize the amount of manual work needed to onboard/offboard an employee and be able to assign permissions automatically - but the must-have is security, especially in sensitive environments operating under regulatory scrutiny. We need to strike a balance between being overly permissive (e.g. unintentionally allowing access to an offboarded employee) and overly restrictive (e.g. getting in the way of a developer's workflow and annoying them with reauthentication requests).

## Where we are Headed

We're currently focused on broader and deeper support for identity management and authentication strategies. Our objective is to allow users to quickly join GitLab with the right level of access, and building support for large organizations to quickly onboard and offboard users is essential.

For large organizations, we also need to make sure that users are armed with the right level of access. GitLab's role-based access control has served small teams well - and fits with a permissions model where anyone can contribute - but makes it challenging for large, security-minded organizations to build the level of granular controls and variation they need.

### What's Next and Why

On theme with the two areas of focus above - deeper support for identity management and fine-grained access controls - Access is immediately focused on these areas:

1. On authentication, Access is prioritizing the availability and capabilities of SAML SSO in GitLab. We've invested and extended the capabilities offered by GitLab's SAML SSO for Groups - particularly important for users using GitLab.com - with improvements like [SCIM](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html#scim-provisioning-using-saml-sso-for-groups-silver-only), [SSO enforcement](https://docs.gitlab.com/ee/user/group/saml_sso/#sso-enforcement), and [group-managed accounts](https://docs.gitlab.com/ee/user/group/saml_sso/#group-managed-accounts).

Current and near future epics:
* [SAML SSO Capabilities](https://gitlab.com/groups/gitlab-org/-/epics/1986)
* [IDP-specific Support](https://gitlab.com/groups/gitlab-org/-/epics/1900)
* [SSO Stability and Availability](https://gitlab.com/groups/gitlab-org/-/epics/1785)

2. On access management, we're prioritizing improvements that help provide much-needed flexibility. Moving from our role-based access control system to a more fine-grained one is challenging, and our current path is considering an attribute-based approach that serves as a control layer on top of roles. We're also working on some challenging engineering work to make membership inheritance in subgroups optional - enabling the flexibility organizations need when managing complicated group structures in GitLab.

Issues in progress or for the very near future:
* [Optionally disabling subgroup membership inheritance](https://gitlab.com/gitlab-org/gitlab/issues/33534)
* [Policies ABAC MVC](https://gitlab.com/gitlab-org/gitlab/issues/7626)

You can see more details on our roadmap in our [Maturity Plan](#maturity-plan) below.

## Maturity Plan

This category is currently **Complete**. The next step in our maturity plan is achieving a **Lovable** state. 
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.
* Note that a `Complete` state does not mean a category is "finished" and is no longer a priority. Even categories that are considered `Lovable` require continued investment.

While authentication and authorization in GitLab has a sufficient feature set to be competitive, we see significant opportunity to elevate our capabilities - especially for the enterprise. You can see the current scope of work to achieve this state in [the corresponding Lovable maturity epic](https://gitlab.com/groups/gitlab-org/-/epics/1964). 
