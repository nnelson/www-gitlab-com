---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2020-05-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 133       | 10.45%          |
| Based in EMEA                               | 340       | 26.71%          |
| Based in LATAM                              | 18        | 1.41%           |
| Based in NORAM                              | 782       | 61.43%          |
| **Total Team Members**                      | **1,273** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 892       | 70.07%          |
| Women                                       | 381       | 29.93%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,273** | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 66        | 75.86%          |
| Women in Leadership                         | 21        | 24.14%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **87**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 439       | 81.60%          |
| Women in Development                        | 99        | 18.40%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **538**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.27%           |
| Asian                                       | 48        | 6.55%           |
| Black or African American                   | 21        | 2.86%           |
| Hispanic or Latino                          | 41        | 5.59%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 26        | 3.55%           |
| White                                       | 443       | 60.44%          |
| Unreported                                  | 152       | 20.74%          |
| **Total Team Members**                      | **733**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 19        | 8.96%           |
| Black or African American                   | 4         | 1.89%           |
| Hispanic or Latino                          | 9         | 4.25%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 3.30%           |
| White                                       | 135       | 63.68%          |
| Unreported                                  | 38        | 17.92%          |
| **Total Team Members**                      | **212**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 8         | 11.76%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 2.94%           |
| White                                       | 43        | 63.24%          |
| Unreported                                  | 15        | 22.06%          |
| **Total Team Members**                      | **68**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.16%           |
| Asian                                       | 114       | 8.96%           |
| Black or African American                   | 31        | 2.44%           |
| Hispanic or Latino                          | 65        | 5.11%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 36        | 2.83%           |
| White                                       | 714       | 56.09%          |
| Unreported                                  | 311       | 24.43%          |
| **Total Team Members**                      | **1,273** | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 55        | 10.22%          |
| Black or African American                   | 9         | 1.67%           |
| Hispanic or Latino                          | 26        | 4.83%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 14        | 2.60%           |
| White                                       | 303       | 56.32%          |
| Unreported                                  | 121       | 24.35%          |
| **Total Team Members**                      | **538**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 10.34%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.15%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 2.30%           |
| White                                       | 53        | 60.92%          |
| Unreported                                  | 22        | 25.29%          |
| **Total Team Members**                      | **87**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 19        | 1.49%           |
| 25-29                                       | 233       | 18.30%          |
| 30-34                                       | 345       | 27.10%          |
| 35-39                                       | 275       | 21.60%          |
| 40-49                                       | 275       | 21.60%          |
| 50-59                                       | 112       | 8.80%           |
| 60+                                         | 14        | 1.10%           |
| Unreported                                  | 0         | 0%              |
| **Total Team Members**                      | **1,273** | **100%**        |


Source: GitLab's HRIS, BambooHR
